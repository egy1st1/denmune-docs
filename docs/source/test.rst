Test
=====

Section 1
----------

:fieldname: Field content

Section 2
----------

.. code-block::
   :caption: A cool example

       The output of this line starts with four spaces.

.. code-block::

       The output of this line has no spaces at the beginning.

	   
	   

Section 3
----------
A cool bit of code::

   Some cool Code

.. code-block:: rst

   A bit of **rst** which should be *highlighted* properly.

   

