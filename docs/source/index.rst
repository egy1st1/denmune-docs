Welcome to DenMune's documentation!
====================================

DenMune Clustering Algorithm
	A clustering algorithm that can find clusters of arbitrary size, shapes and densities in two-dimensions. Higher dimensions are first reduced to 2-D using the t-sne. The algorithm relies on a single parameter K (the number of nearest neighbors). The results show the superiority of DenMune. Enjoy the simplicty but the power of DenMune.

Check out the :doc:`installation` section for further information, including
how to :ref:`installation` the algorithm and use it.

.. note::

   This documentation associated with the paper "DenMune: Density peak based clustering using mutual nearest neighbors"
   
   DOI: https://doi.org/10.1016/j.patcog.2020.107589
   
   Source code is maintained at https://github.com/egy1st/denmune-clustering-algorithm
   
   

User Guide / Tutorial
------------------------

.. toctree::
   :maxdepth: 2

   installation
  

Tutorials and Examples
----------------------------

.. toctree::
   :maxdepth: 1

Real Datasets
----------------

.. toctree::
   :maxdepth: 1
   
   iris_dataset
   digits_dataset
   chars_dataset
   mnist_dataset
   
   
   
Synthestic Datasets
-------------------
   
.. toctree::
   :maxdepth: 1
   
   aggregation_dataset
   a1_dataset
   a2_dataset
   s1_dataset
   s2_dataset
   r15_dataset
   d31_dataset
   unbalance_dataset
   clusterable_dataset
   flame_dataset
   compound_dataset
   jain_dataset
   mouse_dataset
   pathbased_dataset
   spiral_dataset
   dim032_dataset
   dim128_dataset
   dim512_dataset
   g2-2-10_dataset
   g2-2-30_dataset
   g2-2-50_dataset
   varydensity_dataset
  
   

Chameleon's Datasets
----------------------
   
.. toctree::
   :maxdepth: 1
   
   chameleon_ds1_dataset
   chameleon_ds2_dataset
   chameleon_ds3_dataset
   chameleon_ds4_dataset